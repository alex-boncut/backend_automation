package stepDef.databaseStepDef;

import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import org.testng.Assert;
import utils.DBUtils;

import java.math.BigDecimal;
import java.math.BigInteger;

public class databaseStepDef {
    static String mainQuery;

    @Given("user is able to connect to database")
    public void userIsAbleToConnectToDatabase() {
        DBUtils.createDBConnection();
    }

    @When("user send {string} to database")
    public void userSendToDatabase(String query) {
        mainQuery = query;
        DBUtils.executeQuery(query);
    }

    @Then("Validate the {int}")
    public void validateTheSalary(Integer salary) {
        Assert.assertEquals(DBUtils.getCellValue(mainQuery), new BigDecimal(salary));
    }
}
