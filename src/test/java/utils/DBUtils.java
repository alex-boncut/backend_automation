package utils;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

public class DBUtils {
    private static final String url = ConfigReader.getProperty("dbURL");
    private static final String username = ConfigReader.getProperty("dbUsername");
    private static final String password = ConfigReader.getProperty("dbPassword");
    private static Connection connection;
    private static Statement statement;
    private static ResultSet resultSet;

    public static void createDBConnection() {
        try {
            // Create a connection to the database with the parameters stored
            connection = DriverManager.getConnection(url, username, password);
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public static void executeQuery(String query) {
        try {
            // Statement keeps the connection between DB and Automation to send query
            statement = connection.createStatement();
            // ResultSet is sending the query to the database and get the result
            resultSet = statement.executeQuery(query);
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public static List<String> getColumnNames(String query) {
        List<String> columnNames = new ArrayList<String>();

        executeQuery(query);

        try {
            ResultSetMetaData metaData = resultSet.getMetaData();
            int columnCount = metaData.getColumnCount();

            for (int i = 1; i <= columnCount; i++) {
                String columnName = metaData.getColumnName(i);
                columnNames.add(columnName);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return columnNames;
    }

    public static List<List<Object>> getQueryResultList(String query) {
        executeQuery(query);
        List<List<Object>> rowList = new ArrayList<>();

        ResultSetMetaData resultSetMetaData;

        try {
            // This is giving us table information
            resultSetMetaData = resultSet.getMetaData();
            while(resultSet.next()) {
                // Create empty list for each row
                List<Object> row = new ArrayList<>();
                // resultSetMetaData.getColumnCount() giving us the number of columns
                for(int i = 1; i <= resultSetMetaData.getColumnCount(); i++) {
                    row.add(resultSet.getObject(i));
                }
                // we get all information and store it back in list of lists
                rowList.add(row);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }

        return rowList;
    }

    public static Object getCellValue(String query){
        /**
         * if we have only one value from one query we use this method because
         * we don't need list of list
         */
        return getQueryResultList(query).get(0).get(0);
    }
}
