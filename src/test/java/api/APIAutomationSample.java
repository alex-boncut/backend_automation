package api;

import com.github.javafaker.Faker;
import io.restassured.RestAssured;
import io.restassured.response.Response;
import org.testng.Assert;

public class APIAutomationSample {
    public static void main(String[] args) {
        /**
         * Response is an interface that comes from RestAssured library
         * The Response variable "response" stores all the components of API calls
         * including the request and response
         * RestAssured is written with BDD flow
         */
        Response response;

        Faker faker = new Faker();

        // Create user
        response = RestAssured
                .given().log().all()
                .header("Content-Type", "application/json")
                .header("Authorization", "Bearer 82e648c9184caa17b36b3591357864868e4bcdcae3faf9adf2c53ffbe7c29c11")
                .body("{\n" +
                        "    \"name\": \""+ faker.funnyName().name() +"\",\n" +
                        "    \"gender\": \"male\",\n" +
                        "    \"email\": \""+ faker.internet().emailAddress() +"\",\n" +
                        "    \"status\": \"active\"\n" +
                        "}")
                .when().post("https://gorest.co.in/public/v2/users")
                .then().log().all().extract().response();


        // Get user
        int postID = response.jsonPath().getInt("id");

        response = RestAssured
                .given().log().all()
                .header("Content-Type", "application/json")
                .header("Authorization", "Bearer 82e648c9184caa17b36b3591357864868e4bcdcae3faf9adf2c53ffbe7c29c11")
                .when().get("https://gorest.co.in/public/v2/users/"+postID)
                .then().log().all().extract().response();

        // Get all users from tech-global
//        response = RestAssured
//                .given().log().all()
//                .header("Content-Type", "application/json")
//                .header("Authorization", "Bearer 82e648c9184caa17b36b3591357864868e4bcdcae3faf9adf2c53ffbe7c29c11")
//                .when().get("https://gorest.co.in/public/v2/users")
//                .then().log().all().extract().response();

        // Update with PUT user
        response = RestAssured
                .given().log().all()
                .header("Content-Type", "application/json")
                .header("Authorization", "Bearer 82e648c9184caa17b36b3591357864868e4bcdcae3faf9adf2c53ffbe7c29c11")
                .body("{\n" +
                        "    \"name\": \""+ faker.funnyName().name() +"\",\n" +
                        "    \"gender\": \"male\",\n" +
                        "    \"email\": \""+ faker.internet().emailAddress() +"\",\n" +
                        "    \"status\": \"active\"\n" +
                        "}")
                .when().put("https://gorest.co.in/public/v2/users/"+postID)
                .then().log().all().extract().response();

        // Update with PATCH user
        response = RestAssured
                .given().log().all()
                .header("Content-Type", "application/json")
                .header("Authorization", "Bearer 82e648c9184caa17b36b3591357864868e4bcdcae3faf9adf2c53ffbe7c29c11")
                .body("{\"name\": \""+ faker.funnyName().name() +"\"}")
                .when().patch("https://gorest.co.in/public/v2/users/"+postID)
                .then().log().all().extract().response();

        int patchID = response.jsonPath().getInt("id");
        Assert.assertEquals(postID, patchID);

        // DELETE user
        response = RestAssured
                .given().log().all()
                .header("Content-Type", "application/json")
                .header("Authorization", "Bearer 82e648c9184caa17b36b3591357864868e4bcdcae3faf9adf2c53ffbe7c29c11")
                .when().delete("https://gorest.co.in/public/v2/users/"+postID)
                .then().log().all().extract().response();
    }
}
