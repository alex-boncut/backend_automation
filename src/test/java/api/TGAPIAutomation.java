package api;

import com.github.javafaker.Faker;
import io.restassured.RestAssured;
import io.restassured.response.Response;
import org.testng.Assert;

import java.text.SimpleDateFormat;

public class TGAPIAutomation {
    public static void main(String[] args) {
        Response response;

        Faker faker = new Faker();

        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd"); // create formatter
        String birthday = sdf.format(faker.date().birthday()); // generate and format birthday

        // Create user
        response = RestAssured
                .given().log().all()
                .header("Content-Type", "application/json")
                .body("{\n" +
                        "    \"firstName\": \""+ faker.name().firstName() +"\",\n" +
                        "    \"lastName\": \""+ faker.name().lastName() +"\",\n" +
                        "    \"email\": \""+ faker.internet().emailAddress() +"\",\n" +
                        "    \"dob\": \""+ birthday +"\"\n" +
                        "}")
                .when().post("https://tech-global-training.com/students")
                .then().log().all().extract().response();

        // Get user
        int postID = response.jsonPath().getInt("id");

        response = RestAssured
                .given().log().all()
                .header("Content-Type", "application/json")
                .when().get("https://tech-global-training.com/students/"+postID)
                .then().log().all().extract().response();

        // Get all users
//        response = RestAssured
//                .given().log().all()
//                .header("Content-Type", "application/json")
//                .when().get("https://tech-global-training.com/students")
//                .then().log().all().extract().response();

        // Update with PUT user
        response = RestAssured
                .given().log().all()
                .header("Content-Type", "application/json")
                .body("{\n" +
                        "    \"firstName\": \""+ faker.name().firstName() +"\",\n" +
                        "    \"lastName\": \""+ faker.name().lastName() +"\",\n" +
                        "    \"email\": \""+ faker.internet().emailAddress() +"\",\n" +
                        "    \"dob\": \""+ birthday +"\"\n" +
                        "}")
                .when().put("https://tech-global-training.com/students/"+ postID)
                .then().log().all().extract().response();

        // Update with PATCH user
        response = RestAssured
                .given().log().all()
                .header("Content-Type", "application/json")
                .body("{\"firstName\": \""+ faker.name().firstName() +"\"}")
                .when().patch("https://tech-global-training.com/students/"+ postID)
                .then().log().all().extract().response();

        int patchID = response.jsonPath().getInt("id");
        Assert.assertEquals(postID, patchID);

        // DELETE user
        response = RestAssured
                .given().log().all()
                .header("Content-Type", "application/json")
                .when().delete("https://tech-global-training.com/students/"+postID)
                .then().log().all().extract().response();
    }
}
