package database;

import java.sql.*;

public class DatabaseConnection {
    public static void main(String[] args) throws SQLException {
        /*
        * In order to connect ot the database, we need ourURL, username, password and query
        * NOTE: This can be the interview question
         */

        // Create a connection to the database with the parameters stored
//        Connection connection = DriverManager.getConnection(url, username, password);

        // Statement keeps the connection between DB and Automation to send query
//        Statement statement = connection.createStatement();

        // ResultSet is sending the query to the database and get the result
//        ResultSet resultSet = statement.executeQuery(query);

        // ResultSetMetaData gives the information about the table
        // You can't simply print the column values. We need to call them with iterations
//        ResultSetMetaData resultMetaData = resultSet.getMetaData();

    }
}
